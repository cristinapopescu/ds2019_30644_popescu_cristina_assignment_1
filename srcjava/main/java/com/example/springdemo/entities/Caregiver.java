package com.example.springdemo.entities;

import javax.persistence.*;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "caregiver")
public class Caregiver {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @Column(name = "name", length = 100)
    private String name;

    @Column(name = "birthDate", length = 100)
    private Timestamp birthDate;

    @Column(name = "gender", length = 100)
    private String gender;

    @Column(name = "address", unique=true, length = 200)
    private String address;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_user", unique = true, referencedColumnName = "id")
    private User user;

    @OneToMany(mappedBy = "caregiver", fetch = FetchType.LAZY)
    private List<Patient> patients;

    public Caregiver() {
    }

    public Caregiver(Integer id, String name, Timestamp birthDate, String gender, String address) {
        this.id = id;
        this.name = name;
        this.birthDate = birthDate;
        this.gender = gender;
        this.address = address;

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Timestamp getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Timestamp birthDate) {
        this.birthDate = birthDate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<Patient> getPatients() {
        return patients;
    }

    public void setPatients(List<Patient> patients) {
        this.patients = patients;
    }
}
