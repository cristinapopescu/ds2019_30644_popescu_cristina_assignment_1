package com.example.springdemo.services;

import com.example.springdemo.dto.IntakeDTO;
import com.example.springdemo.entities.Intake;
import com.example.springdemo.entities.Medication;
import com.example.springdemo.entities.Patient;
import com.example.springdemo.repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class IntakeService {

    private final PatientRepository patientRepository;
    private final MedicationRepository medicationRepository;
    private final IntakeRepository intakeRepository;

    @Autowired
    public IntakeService(PatientRepository patientRepository, MedicationRepository medicationRepository, IntakeRepository intakeRepository) {
        this.patientRepository = patientRepository;
        this.medicationRepository = medicationRepository;
        this.intakeRepository = intakeRepository;
    }

    public Integer insert(IntakeDTO intakeDTO) {
        Optional<Patient> patient = patientRepository.findById(intakeDTO.getPatientId());
        Optional<Medication> medication = medicationRepository.findById(intakeDTO.getMedicationId());
        Intake intake = new Intake(intakeDTO.getStartDate(), intakeDTO.getEndDate());

        intake.setMedication(medication.get());
        intake.setPatient(patient.get());
        return intakeRepository
                .save(intake)
                .getId();
    }
}
