package com.example.springdemo.services;

import com.example.springdemo.dto.CaregiverDTO;
import com.example.springdemo.dto.CaregiverWithPatientsDTO;
import com.example.springdemo.dto.PatientDTO;
import com.example.springdemo.dto.builders.CaregiverBuilder;
import com.example.springdemo.dto.builders.CaregiverWithPatientsBuilder;
import com.example.springdemo.dto.builders.PatientBuilder;
import com.example.springdemo.entities.Caregiver;
import com.example.springdemo.entities.Patient;
import com.example.springdemo.entities.User;
import com.example.springdemo.repositories.CaregiverRepository;
import com.example.springdemo.repositories.UserRepository;
import com.example.springdemo.validators.CaregiverFieldValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CaregiverService {

    private final CaregiverRepository caregiverRepository;
    private final UserRepository userRepository;

    @Autowired
    public CaregiverService(CaregiverRepository caregiverRepository, UserRepository userRepository) {
        this.caregiverRepository = caregiverRepository;
        this.userRepository = userRepository;
    }

    public CaregiverDTO findCaregiverById(Integer id){
        Optional<Caregiver> caregiver  = caregiverRepository.findById(id);

        if (!caregiver.isPresent()) {
            //throw new ResourceNotFoundException("Patient", "user id", id);
        }
        return CaregiverBuilder.generateDTOFromEntity(caregiver.get());
    }

    public List<CaregiverDTO> findAll(){
        List<Caregiver> caregivers = caregiverRepository.getAllOrdered();

        return caregivers.stream()
                .map(CaregiverBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());
    }

    public List<PatientDTO> findAllPatients(Integer id) {
        Optional<Caregiver> caregiverId = caregiverRepository.getCaregiverByUserId(id);
        List<Patient> caregiverList = caregiverRepository.getAllPatients(caregiverId.get().getId());

        return caregiverList.stream().map(PatientBuilder::generateDTOFromEntity).collect(Collectors.toList());
    }

    public Integer insert(CaregiverDTO caregiverDTO) {
        CaregiverFieldValidator.validateInsertOrUpdate(caregiverDTO);
        Caregiver caregiver = CaregiverBuilder.generateEntityFromDTO(caregiverDTO);
        User user = new User(caregiverDTO.getUsername(), caregiverDTO.getPassword(), "caregiver");
        caregiver.setUser(user);
        userRepository.save(user);
        return caregiverRepository
                .save(caregiver)
                .getId();
    }

    public Integer update(CaregiverDTO caregiverDTO) {
        CaregiverFieldValidator.validateInsertOrUpdate(caregiverDTO);
        Optional<Caregiver> caregiver = caregiverRepository.findById(caregiverDTO.getId());
        User user = caregiver.get().getUser();
        Caregiver caregiver1 = CaregiverBuilder.generateEntityFromDTO(caregiverDTO);
        caregiver1.setUser(user);
        return caregiverRepository.save(caregiver1).getId();
    }

    public void delete(Integer id){
        this.caregiverRepository.deleteById(id);
    }

}
