package com.example.springdemo.repositories;

import com.example.springdemo.entities.Caregiver;
import com.example.springdemo.entities.Patient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CaregiverRepository extends JpaRepository<Caregiver, Integer> {


    @Query(value = "SELECT u FROM Caregiver u WHERE u.user.id = ?1"
    )
    Optional<Caregiver> getCaregiverByUserId(Integer id);

    @Query(value = "SELECT u " +
            "FROM Caregiver u " +
            "ORDER BY u.name")
    List<Caregiver> getAllOrdered();

    @Query(value = "SELECT p " +
            "FROM Caregiver p " +
            "INNER JOIN FETCH p.patients i"
    )
    List<Caregiver> getAllFetch();

    @Query(value = "SELECT p " +
            "FROM Patient p " +
            "WHERE p.caregiver.id = ?1"
    )
    List<Patient> getAllPatients(Integer id);
}
