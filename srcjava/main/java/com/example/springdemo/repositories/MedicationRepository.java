package com.example.springdemo.repositories;

import com.example.springdemo.entities.Medication;
import com.example.springdemo.entities.Patient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MedicationRepository extends JpaRepository<Medication, Integer> {
    Medication findByName(String name);

    @Query(value = "SELECT u " +
            "FROM Medication u " +
            "ORDER BY u.name")
    List<Medication> getAllOrdered();

}
