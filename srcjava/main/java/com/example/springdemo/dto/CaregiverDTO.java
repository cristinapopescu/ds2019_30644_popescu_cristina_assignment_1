package com.example.springdemo.dto;

import com.example.springdemo.entities.Caregiver;

import java.sql.Time;
import java.sql.Timestamp;
import java.util.List;
import java.util.Objects;

public class CaregiverDTO {

    private Integer id;
    private String name;
    private Timestamp birthDate;
    private String gender;
    private String address;
    private String username;
    private String password;

    public CaregiverDTO() {
    }

    public CaregiverDTO(Integer id, String name, Timestamp birthDate, String gender, String address) {
        this.id = id;
        this.name = name;
        this.birthDate = birthDate;
        this.gender = gender;
        this.address = address;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Timestamp getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Timestamp birthDate) {
        this.birthDate = birthDate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CaregiverDTO caregiverDTO = (CaregiverDTO) o;
        return Objects.equals(id, caregiverDTO.id) &&
                Objects.equals(name, caregiverDTO.name) &&
                Objects.equals(birthDate, caregiverDTO.birthDate) &&
                Objects.equals(gender, caregiverDTO.gender) &&
                Objects.equals(address, caregiverDTO.address);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, name, birthDate, gender, address);
    }
}
