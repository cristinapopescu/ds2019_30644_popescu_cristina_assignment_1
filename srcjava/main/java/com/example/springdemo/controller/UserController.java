package com.example.springdemo.controller;

import com.example.springdemo.dto.UserDTO;
import com.example.springdemo.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/login")
@CrossOrigin()
public class UserController {
    @Autowired
    private UserService userService;


    @PostMapping()
    public UserDTO login(@RequestBody UserDTO userDTO) {
        return userService.logIn(userDTO.getUsername(), userDTO.getPassword());
    }
}