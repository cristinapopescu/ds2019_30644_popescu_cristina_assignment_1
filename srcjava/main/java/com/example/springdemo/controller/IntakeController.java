package com.example.springdemo.controller;

import com.example.springdemo.dto.IntakeDTO;
import com.example.springdemo.entities.Intake;
import com.example.springdemo.services.IntakeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping(value = "/intake")
public class IntakeController {

    private final IntakeService intakeService;

    @Autowired
    public IntakeController(IntakeService intakeService) {
        this.intakeService = intakeService;
    }

    @PostMapping()
    public Integer insertPatientDTO(@RequestBody IntakeDTO intake) {
        return intakeService.insert(intake);
    }
}