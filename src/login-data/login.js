import React, {useState} from 'react';
import { withRouter } from 'react-router-dom';
import ApiService from '../api-service'
import Redirect from "react-router-dom/Redirect";

class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: '',
            submitted: ''
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(e) {
        this.setState({ [e.target.name] : e.target.value});
    }

    handleSubmit(e) {
        e.preventDefault();
    }

    logIn = () => {
        let data = { username : this.state.username, password  : this.state.password}
        ApiService.login(data).then(
            response => {
                if (response.data.role === "doctor") {
                    this.setState({submitted: "doctor"})
                    localStorage.setItem('role', "doctor")
                    localStorage.setItem('userId', response.data.id)
                }
                if (response.data.role === "patient") {
                    this.setState({submitted: "patient"})
                    localStorage.setItem('role', "patient")
                    localStorage.setItem('userId', response.data.id)
                }
                if (response.data.role === "caregiver") {
                    this.setState({submitted: "caregiver"})
                    localStorage.setItem('role', "caregiver")
                    localStorage.setItem('userId', response.data.id)
                }
            }
        )
    }

    redirect = () => {
        if (this.state.submitted === "doctor") {
            return <Redirect to={'/persons'} />
        }
        if (this.state.submitted === "patient") {
            return <Redirect to={'/patient'} />
        }
        if (this.state.submitted === "caregiver") {
            return <Redirect to={'/caregiver'} />
        }
    }

    render() {
        const { loggingIn } = this.props;
        const { username, password, submitted } = this.state;
        return (
            <div className="col-md-6 col-md-offset-3">
                <h2>Login</h2>
                {this.redirect()}
                <form name="form" onSubmit={this.handleSubmit}>
                    <div className="username">
                        <label>Username</label>
                        <input type="text" name="username" onChange={this.handleChange} />

                    </div>
                    <div className="pass">
                        <label >Password</label>
                        <input type="password" name="password" onChange={this.handleChange} />
                    </div>
                    <div className="form-group">
                            <button className="btn btn-primary" onClick={this.logIn}>Login</button>
                        {loggingIn &&
                        <img src="data:image/gif;base64,R0lGODlhEAAQAPIAAP///wAAAMLCwkJCQgAAAGJiYoKCgpKSkiH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAEAAQAAADMwi63P4wyklrE2MIOggZnAdOmGYJRbExwroUmcG2LmDEwnHQLVsYOd2mBzkYDAdKa+dIAAAh+QQJCgAAACwAAAAAEAAQAAADNAi63P5OjCEgG4QMu7DmikRxQlFUYDEZIGBMRVsaqHwctXXf7WEYB4Ag1xjihkMZsiUkKhIAIfkECQoAAAAsAAAAABAAEAAAAzYIujIjK8pByJDMlFYvBoVjHA70GU7xSUJhmKtwHPAKzLO9HMaoKwJZ7Rf8AYPDDzKpZBqfvwQAIfkECQoAAAAsAAAAABAAEAAAAzMIumIlK8oyhpHsnFZfhYumCYUhDAQxRIdhHBGqRoKw0R8DYlJd8z0fMDgsGo/IpHI5TAAAIfkECQoAAAAsAAAAABAAEAAAAzIIunInK0rnZBTwGPNMgQwmdsNgXGJUlIWEuR5oWUIpz8pAEAMe6TwfwyYsGo/IpFKSAAAh+QQJCgAAACwAAAAAEAAQAAADMwi6IMKQORfjdOe82p4wGccc4CEuQradylesojEMBgsUc2G7sDX3lQGBMLAJibufbSlKAAAh+QQJCgAAACwAAAAAEAAQAAADMgi63P7wCRHZnFVdmgHu2nFwlWCI3WGc3TSWhUFGxTAUkGCbtgENBMJAEJsxgMLWzpEAACH5BAkKAAAALAAAAAAQABAAAAMyCLrc/jDKSatlQtScKdceCAjDII7HcQ4EMTCpyrCuUBjCYRgHVtqlAiB1YhiCnlsRkAAAOwAAAAAAAAAAAA==" />
                        }
                    </div>
                </form>
            </div>
        );
    }
}

export default Login;