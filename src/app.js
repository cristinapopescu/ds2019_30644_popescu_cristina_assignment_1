import React from 'react'
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import NavigationBar from './navigation-bar'
import Home from './home/home';
import Persons from './person-data/person/persons'
import Login from './login-data/login'
import CaregiverPatients from './caregiver-patients'

import ErrorPage from './commons/errorhandling/error-page';
import styles from './commons/styles/project-style.css';

let enums = require('./commons/constants/enums');

class App extends React.Component {


    render() {

        return (
            <div className={styles.back}>
            <Router>
                <div>
                    <NavigationBar />
                    <Switch>

                        <Route
                            exact
                            path='/'
                            render={() => <Home/>}
                        />

                        <Route
                            exact
                            path='/persons'
                            render={() => <Persons/>}
                        />
                        <Route
                            exact
                            path='/login'
                            render={() => <Login/>}
                        />
                        <Route
                            exact
                            path='/caregiver'
                            render={() => <CaregiverPatients/>}
                        />
                        {/*Error*/}
                        <Route
                            exact
                            path='/error'
                            render={() => <ErrorPage/>}
                        />

                        <Route render={() =><ErrorPage/>} />
                    </Switch>
                </div>
            </Router>
            </div>
        )
    };
}

export default App
