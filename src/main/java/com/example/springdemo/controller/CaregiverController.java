package com.example.springdemo.controller;


import com.example.springdemo.dto.CaregiverDTO;
import com.example.springdemo.dto.CaregiverWithPatientsDTO;
import com.example.springdemo.dto.PatientDTO;
import com.example.springdemo.entities.Patient;
import com.example.springdemo.services.CaregiverService;
import com.example.springdemo.services.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/caregiver")
public class CaregiverController {

    private final CaregiverService caregiverService;

    @Autowired
    public CaregiverController(CaregiverService caregiverService) {
        this.caregiverService = caregiverService;
    }
//    @GetMapping(value = "/{id}")
//    public CaregiverDTO findById(@PathVariable("id") Integer id){
//        return caregiverService.findCaregiverById(id);
//    }
    @GetMapping()
    public List<CaregiverDTO> findAll(){
    return caregiverService.findAll();
}

    @GetMapping(value = "/{id}")

    public List<PatientDTO> findAll(@PathVariable Integer id) {
        System.out.println("mata are cratima" + id);
        return caregiverService.findAllPatients(id);
    }

    @PostMapping()
    public Integer insertCaregiverDTO(@RequestBody CaregiverDTO caregiverDTO){
        return caregiverService.insert(caregiverDTO);
    }

    @PutMapping()
    public Integer updateCaregiver(@RequestBody CaregiverDTO caregiverDTO) {
        return caregiverService.update(caregiverDTO);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Integer id){
        caregiverService.delete(id);
    }
}
