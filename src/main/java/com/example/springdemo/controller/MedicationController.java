package com.example.springdemo.controller;


import com.example.springdemo.dto.CaregiverDTO;
import com.example.springdemo.dto.CaregiverWithPatientsDTO;
import com.example.springdemo.dto.MedicationDTO;
import com.example.springdemo.dto.PatientDTO;
import com.example.springdemo.services.CaregiverService;
import com.example.springdemo.services.MedicationService;
import com.example.springdemo.services.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/medication")
public class MedicationController {

    private final MedicationService medicationService;

    @Autowired
    public MedicationController(MedicationService medicationService) {
        this.medicationService = medicationService;
    }

    @GetMapping()
    public List<MedicationDTO> findAll(){
        return medicationService.findAll();
    }

    @GetMapping(value = "/{id}")
    public MedicationDTO findById(@PathVariable("id") Integer id){
        return medicationService.findMedicationById(id);
    }

    @PostMapping()
    public Integer insertMedicationDTO(@RequestBody MedicationDTO medicationDTO){
        return medicationService.insert(medicationDTO);
    }

    @PutMapping()
    public Integer updateMedication(@RequestBody MedicationDTO medicationDTO) {
        return medicationService.update(medicationDTO);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Integer id){
        medicationService.delete(id);
    }
}
