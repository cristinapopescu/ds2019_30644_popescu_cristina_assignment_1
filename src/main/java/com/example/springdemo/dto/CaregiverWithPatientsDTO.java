package com.example.springdemo.dto;

import com.example.springdemo.dto.builders.PatientBuilder;
import com.example.springdemo.entities.Caregiver;
import com.example.springdemo.entities.Patient;

import java.util.List;
import java.util.stream.Collectors;

public class CaregiverWithPatientsDTO {
    private Integer id;
    private List<PatientDTO> patients;

    public CaregiverWithPatientsDTO() {}
    public CaregiverWithPatientsDTO(Integer id, List<PatientDTO> patients) {

        this.patients = patients;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<PatientDTO> getPatients() {
        return patients;
    }

    public void setPatients(List<PatientDTO> patients) {
        this.patients = patients;
    }
}
