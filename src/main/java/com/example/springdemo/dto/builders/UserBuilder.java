package com.example.springdemo.dto.builders;


import com.example.springdemo.dto.UserDTO;
import com.example.springdemo.entities.User;

public class UserBuilder {
    private UserBuilder() {
    }

    public static UserDTO generateDTOFromEntity(User patient){
        return new UserDTO(
                patient.getId(),
                patient.getUsername(),
                patient.getPassword(),
                patient.getRole());
    }

    public static User generateEntityFromDTO(UserDTO patientDTO){
        return new User(
                patientDTO.getUsername(),
                patientDTO.getPassword(),
                patientDTO.getRole());
    }
}
