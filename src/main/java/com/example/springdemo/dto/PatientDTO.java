package com.example.springdemo.dto;

import com.example.springdemo.entities.Caregiver;
import com.example.springdemo.entities.User;

import java.sql.Time;
import java.sql.Timestamp;
import java.util.Objects;

public class PatientDTO {

    private Integer id;
    private String name;
    private Timestamp birthDate;
    private String gender;
    private String address;
    private String medicalRecord;
    private String username;
    private String password;
    private Integer idCaregiver;

    public PatientDTO() {
    }

    public PatientDTO(Integer id, String name, Timestamp birthDate, String gender, String address, String medicalRecord) {
        this.id = id;
        this.name = name;
        this.birthDate = birthDate;
        this.gender = gender;
        this.address = address;
        this.medicalRecord = medicalRecord;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Timestamp getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Timestamp birthDate) {
        this.birthDate = birthDate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMedicalRecord() {
        return medicalRecord;
    }

    public void setMedicalRecord(String medicalRecord) {
        this.medicalRecord = medicalRecord;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getIdCaregiver() {
        return idCaregiver;
    }

    public void setIdCaregiver(Integer id) {
        this.idCaregiver = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PatientDTO patientDTO = (PatientDTO) o;
        return Objects.equals(id, patientDTO.id) &&
                Objects.equals(name, patientDTO.name) &&
                Objects.equals(birthDate, patientDTO.birthDate) &&
                Objects.equals(gender, patientDTO.gender) &&
                Objects.equals(address, patientDTO.address) &&
                Objects.equals(medicalRecord, patientDTO.medicalRecord);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, name, birthDate, gender, address, medicalRecord);
    }
}
