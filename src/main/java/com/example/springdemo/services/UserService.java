package com.example.springdemo.services;

import com.example.springdemo.dto.UserDTO;
import com.example.springdemo.dto.builders.UserBuilder;
import com.example.springdemo.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.springdemo.entities.User;

@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public UserDTO logIn(String username, String password) {
        User user = userRepository.findByUsername(username, password);
        System.out.println(user);
        return UserBuilder.generateDTOFromEntity(user);
    }
}
