package com.example.springdemo.services;

import com.example.springdemo.dto.MedicationDTO;
import com.example.springdemo.dto.PatientDTO;
import com.example.springdemo.dto.builders.MedicationBuilder;
import com.example.springdemo.dto.builders.PatientBuilder;
import com.example.springdemo.entities.Medication;
import com.example.springdemo.entities.Patient;
import com.example.springdemo.repositories.MedicationRepository;
import com.example.springdemo.repositories.PatientRepository;
import com.example.springdemo.validators.MedicationFieldValidator;
import com.example.springdemo.validators.PatientFieldValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class MedicationService {

    private final MedicationRepository medicationRepository;

    @Autowired
    public MedicationService(MedicationRepository medicationRepository) {
        this.medicationRepository = medicationRepository;
    }

    public MedicationDTO findMedicationById(Integer id){
        Optional<Medication> medication  = medicationRepository.findById(id);

        if (!medication.isPresent()) {
            //throw new ResourceNotFoundException("Patient", "user id", id);
        }
        return MedicationBuilder.generateDTOFromEntity(medication.get());
    }

    public List<MedicationDTO> findAll(){
        List<Medication> medications = medicationRepository.getAllOrdered();

        return medications.stream()
                .map(MedicationBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());
    }

    public Integer insert(MedicationDTO medicationDTO) {
        MedicationFieldValidator.validateInsertOrUpdate(medicationDTO);

        Medication medication = medicationRepository.findByName(medicationDTO.getName());
        if(medication != null){
            //throw  new DuplicateEntryException("Patient", "name", patientDTO.getName());
        }

        return medicationRepository
                .save(MedicationBuilder.generateEntityFromDTO(medicationDTO))
                .getId();
    }

    public Integer update(MedicationDTO medicationDTO) {

        Optional<Medication> medication = medicationRepository.findById(medicationDTO.getId());

        if(!medication.isPresent()){
            //throw new ResourceNotFoundException("Person", "user id", patientDTO.getId().toString());
        }
        MedicationFieldValidator.validateInsertOrUpdate(medicationDTO);

        return medicationRepository.save(MedicationBuilder.generateEntityFromDTO(medicationDTO)).getId();
    }

    public void delete(Integer id){
        this.medicationRepository.deleteById(id);
    }

}
