package com.example.springdemo.services;

import com.example.springdemo.dto.PatientDTO;
import com.example.springdemo.dto.builders.PatientBuilder;
import com.example.springdemo.entities.Caregiver;
import com.example.springdemo.entities.Patient;
import com.example.springdemo.entities.User;
import com.example.springdemo.repositories.CaregiverRepository;
import com.example.springdemo.repositories.PatientRepository;
import com.example.springdemo.repositories.UserRepository;
import com.example.springdemo.validators.PatientFieldValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class PatientService {

    private final PatientRepository patientRepository;
    private final UserRepository userRepository;
    private final CaregiverRepository caregiverRepository;


    @Autowired
    public PatientService(PatientRepository patientRepository, UserRepository userRepository, CaregiverRepository caregiverRepository) {
        this.patientRepository = patientRepository;
        this.userRepository = userRepository;
        this.caregiverRepository = caregiverRepository;
    }

    public PatientDTO findPatientById(Integer id){
        Optional<Patient> patient  = patientRepository.findById(id);

        return PatientBuilder.generateDTOFromEntity(patient.get());
    }

    public List<PatientDTO> findAll(){
        List<Patient> patients = patientRepository.getAllOrdered();

        return patients.stream()
                .map(PatientBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());
    }

    public Integer insert(PatientDTO patientDTO) {
        PatientFieldValidator.validateInsertOrUpdate(patientDTO);
        Patient patient = PatientBuilder.generateEntityFromDTO(patientDTO);
        User user = new User(patientDTO.getUsername(), patientDTO.getPassword(), "patient");
        Optional<Caregiver> caregiver = caregiverRepository.findById(patientDTO.getIdCaregiver());
        patient.setCaregiver(caregiver.get());
        patient.setUser(user);
        userRepository.save(user);
        return patientRepository
                .save(patient)
                .getId();
    }

    public Integer update(PatientDTO patientDTO) {
        PatientFieldValidator.validateInsertOrUpdate(patientDTO);
        Optional<Patient> patient = patientRepository.findById(patientDTO.getId());
        Optional<Caregiver> caregiver = caregiverRepository.findById(patientDTO.getIdCaregiver());
        User user = patient.get().getUser();
        Patient patient1 = PatientBuilder.generateEntityFromDTO(patientDTO);
        patient1.setUser(user);
        patient1.setCaregiver(caregiver.get());
        return patientRepository.save(patient1).getId();
    }

    public void delete(Integer id){
        this.patientRepository.deleteById(id);
    }

}
