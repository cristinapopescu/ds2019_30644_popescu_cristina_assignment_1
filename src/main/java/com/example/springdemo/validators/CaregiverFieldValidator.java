package com.example.springdemo.validators;

import com.example.springdemo.dto.CaregiverDTO;
import com.example.springdemo.dto.PatientDTO;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.ArrayList;
import java.util.List;

public class CaregiverFieldValidator{

    private static final Log LOGGER = LogFactory.getLog(CaregiverFieldValidator.class);
    //private static final EmailFieldValidator EMAIL_VALIDATOR = new EmailFieldValidator() ;

    private CaregiverFieldValidator(){}

    public static void validateInsertOrUpdate(CaregiverDTO caregiverDTO) {

        List<String> errors = new ArrayList<>();
        if (caregiverDTO == null) {
            errors.add("caregiverDTO is null");
            //throw new IncorrectParameterException(PersonDTO.class.getSimpleName(), errors);
        }

        if (!errors.isEmpty()) {
            LOGGER.error(errors);
            //throw new IncorrectParameterException(PersonFieldValidator.class.getSimpleName(), errors);
        }
    }
}
