package com.example.springdemo.validators;

import com.example.springdemo.dto.PatientDTO;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.ArrayList;
import java.util.List;

public class PatientFieldValidator{

    private static final Log LOGGER = LogFactory.getLog(PatientFieldValidator.class);
    //private static final EmailFieldValidator EMAIL_VALIDATOR = new EmailFieldValidator() ;

    private PatientFieldValidator(){}

    public static void validateInsertOrUpdate(PatientDTO patientDTO) {

        List<String> errors = new ArrayList<>();
        if (patientDTO == null) {
            errors.add("personDTO is null");
            //throw new IncorrectParameterException(PersonDTO.class.getSimpleName(), errors);
        }

        if (!errors.isEmpty()) {
            LOGGER.error(errors);
            //throw new IncorrectParameterException(PersonFieldValidator.class.getSimpleName(), errors);
        }
    }
}
