import React from 'react';
import validate from "./validators/person-validators";
import TextInput from "./fields/TextInput";
import './fields/fields.css';
import Button from "react-bootstrap/Button";
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";
import ApiService from "../../api-service";


class MedicationForm extends React.Component{

    constructor(props){
        super(props);
        this.toggleForm = this.toggleForm.bind(this);

        this.state = {

            errorStatus: 0,
            error: null,

            formIsValid: false,

            formControls : {
                id: {
                    value: '',
                    placeholder: 'Medication id...',
                    valid: true,
                    touched: false,

                },

                name: {
                    value: '',
                    placeholder: 'Medication name...',
                    valid: false,
                    touched: false,
                    validationRules: {
                        minLength: 3,
                        isRequired: true
                    }
                },

                dosage: {
                    value: '',
                    placeholder: '1-3/zi',
                    valid: false,
                    touched: false,
                    validationRules: {
                        isRequired: true
                    }
                },

                sideEffects: {
                    value: '',
                    placeholder: '',
                    valid: false,
                    touched: false,

                },
            }
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmitMedication = this.handleSubmitMedication.bind(this);
        this.deleteMedication = this.deleteMedication.bind(this);
        this.updateMedication = this.updateMedication.bind(this);
    }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }

    componentDidMount() {

    }


    handleChange = event => {

        const name = event.target.name;
        const value = event.target.value;

        const updatedControls = {
            ...this.state.formControls
        };

        const updatedFormElement = {
            ...updatedControls[name]
        };

        updatedFormElement.value = value;
        updatedFormElement.touched = true;
        updatedFormElement.valid = validate(value, updatedFormElement.validationRules);

        console.log("Element: " +  name + " validated: " + updatedFormElement.valid);

        updatedControls[name] = updatedFormElement;

        let formIsValid = true;
        for (let updatedFormElementName in updatedControls) {
            formIsValid = updatedControls[updatedFormElementName].valid && formIsValid;
        }

        this.setState({
            formControls: updatedControls,
            formIsValid: formIsValid
        });
    };

    deleteMedication(id) {
        ApiService.deleteMedication(id);
    }
    updateMedication() {
        let medication = {
            id: this.state.formControls.id.value,
            name: this.state.formControls.name.value,
            dosage: this.state.formControls.dosage.value,
            sideEffects: this.state.formControls.sideEffects.value,
        };
        ApiService.updateMedication(medication);
    }
    handleSubmitMedication(){
        let medication = {
            id: this.state.formControls.id.value,
            name: this.state.formControls.name.value,
            dosage: this.state.formControls.dosage.value,
            sideEffects: this.state.formControls.sideEffects.value
        };
        ApiService.insertMedication(medication);
    }

    render() {
        return (

            <form onSubmit={this.handleSubmitMedication}>

                <h1>Insert new Medication</h1>

                <p> Id: </p>

                <TextInput name="id"
                           placeholder={this.state.formControls.id.placeholder}
                           value={this.state.formControls.id.value}
                           onChange={this.handleChange}
                           touched={this.state.formControls.id.touched}
                           valid={this.state.formControls.id.valid}
                />


                <p> Name: </p>

                <TextInput name="name"
                           placeholder={this.state.formControls.name.placeholder}
                           value={this.state.formControls.name.value}
                           onChange={this.handleChange}
                           touched={this.state.formControls.name.touched}
                           valid={this.state.formControls.name.valid}
                />
                {this.state.formControls.name.touched && !this.state.formControls.name.valid &&
                <div className={"error-message row"}> * Name must have at least 3 characters </div>}

                <p> Dosage: </p>
                <TextInput name="dosage"
                           placeholder={this.state.formControls.dosage.placeholder}
                           value={this.state.formControls.dosage.value}
                           onChange={this.handleChange}
                           touched={this.state.formControls.dosage.touched}
                           valid={this.state.formControls.dosage.valid}
                />
                {this.state.formControls.dosage.touched && !this.state.formControls.dosage.valid &&
                <div className={"error-message row"}> * Name must have at least 3 characters </div>}

                <p> Side Effects: </p>
                <TextInput name="sideEffects"
                           placeholder={this.state.formControls.sideEffects.placeholder}
                           value={this.state.formControls.sideEffects.value}
                           onChange={this.handleChange}
                           touched={this.state.formControls.sideEffects.touched}
                           valid={this.state.formControls.sideEffects.valid}
                />
                {this.state.formControls.sideEffects.touched && !this.state.formControls.sideEffects.valid &&
                <div className={"error-message row"}> * Name must have at least 3 characters </div>}


                <p> </p>
                <Button variant="success"
                        type={"submit"}
                        disabled={!this.state.formIsValid}>
                    Submit
                </Button>
                &nbsp;
                <Button variant="success" onClick={() => this.deleteMedication(this.state.formControls.id.value)}>
                    Delete
                </Button>
                &nbsp;
                <Button variant="success" onClick={() =>
                    this.updateMedication()}>
                    Update
                </Button>

                {this.state.errorStatus > 0 &&
                <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>}

            </form>
        );
    }
}

export default MedicationForm;
