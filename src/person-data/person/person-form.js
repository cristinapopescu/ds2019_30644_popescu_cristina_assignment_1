import React from 'react';
import validate from "./validators/person-validators";
import TextInput from "./fields/TextInput";
import './fields/fields.css';
import Button from "react-bootstrap/Button";
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";
import ApiService from "../../api-service";


class PersonForm extends React.Component{

    constructor(props){
        super(props);
        this.toggleForm = this.toggleForm.bind(this);

        this.state = {

            errorStatus: 0,
            error: null,

            formIsValid: false,

           formControls : {
               id: {
                   value: '',
                   placeholder: 'Caregiver id...',
                   valid: true,
                   touched: false,

               },

               name: {
                   value: '',
                   placeholder: 'Caregiver name...',
                   valid: false,
                   touched: false,
                   validationRules: {
                       minLength: 3,
                       isRequired: true
                   }
               },

               birthDate: {
                   value: '',
                   placeholder: '1997-05-11',
                   valid: false,
                   touched: false,
                   validationRules: {
                       isRequired: true
                   }
               },

               gender: {
                   value: '',
                   placeholder: 'M/F',
                   valid: false,
                   touched: false,

               },
               address: {
                   value: '',
                   placeholder: 'Cluj, Zorilor, Str. Lalelelor 21...',
                   valid: false,
                   touched: false,
               },
               username: {
                   value: '',
                   placeholder: 'username',
                   valid: true,
                   touched: false,
               },
               password: {
                   value: '',
                   placeholder: 'password',
                   valid: true,
                   touched: false,
               },
           }
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmitCaregiver = this.handleSubmitCaregiver.bind(this);
        this.deleteCaregiver = this.deleteCaregiver.bind(this);
        this.updateCaregiver = this.updateCaregiver.bind(this);
    }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }

    componentDidMount() {

    }


    handleChange = event => {

        const name = event.target.name;
        const value = event.target.value;

        const updatedControls = {
            ...this.state.formControls
        };

        const updatedFormElement = {
            ...updatedControls[name]
        };

        updatedFormElement.value = value;
        updatedFormElement.touched = true;
        updatedFormElement.valid = validate(value, updatedFormElement.validationRules);

        console.log("Element: " +  name + " validated: " + updatedFormElement.valid);

        updatedControls[name] = updatedFormElement;

        let formIsValid = true;
        for (let updatedFormElementName in updatedControls) {
            formIsValid = updatedControls[updatedFormElementName].valid && formIsValid;
        }

        this.setState({
            formControls: updatedControls,
            formIsValid: formIsValid
        });
    };

    deleteCaregiver(id) {
        ApiService.deleteCaregiver(id);
    }
    updateCaregiver() {
        let caregiver = {
            id: this.state.formControls.id.value,
            name: this.state.formControls.name.value,
            birthDate: this.state.formControls.birthDate.value,
            gender: this.state.formControls.gender.value,
            address: this.state.formControls.address.value,
        };
        ApiService.updateCaregiver(caregiver);
    }
    handleSubmitCaregiver(){
        let caregiver = {
            id: this.state.formControls.id.value,
            name: this.state.formControls.name.value,
            birthDate: this.state.formControls.birthDate.value,
            gender: this.state.formControls.gender.value,
            address: this.state.formControls.address.value,
            username: this.state.formControls.username.value,
            password: this.state.formControls.password.value
        };
        ApiService.insertCaregiver(caregiver);
    }

    render() {
        return (

          <form onSubmit={this.handleSubmitCaregiver}>

              <h1>Insert new Caregiver</h1>

              <p> Id: </p>

              <TextInput name="id"
                         placeholder={this.state.formControls.id.placeholder}
                         value={this.state.formControls.id.value}
                         onChange={this.handleChange}
                         touched={this.state.formControls.id.touched}
                         valid={this.state.formControls.id.valid}
              />


              <p> Name: </p>

              <TextInput name="name"
                         placeholder={this.state.formControls.name.placeholder}
                         value={this.state.formControls.name.value}
                         onChange={this.handleChange}
                         touched={this.state.formControls.name.touched}
                         valid={this.state.formControls.name.valid}
              />
              {this.state.formControls.name.touched && !this.state.formControls.name.valid &&
              <div className={"error-message row"}> * Name must have at least 3 characters </div>}

              <p> Birth date: </p>
              <TextInput name="birthDate"
                         placeholder={this.state.formControls.birthDate.placeholder}
                         value={this.state.formControls.birthDate.value}
                         onChange={this.handleChange}
                         touched={this.state.formControls.birthDate.touched}
                         valid={this.state.formControls.birthDate.valid}
              />
              {this.state.formControls.birthDate.touched && !this.state.formControls.birthDate.valid &&
              <div className={"error-message row"}> * Name must have at least 3 characters </div>}

              <p> Gender: </p>
              <TextInput name="gender"
                         placeholder={this.state.formControls.gender.placeholder}
                         value={this.state.formControls.gender.value}
                         onChange={this.handleChange}
                         touched={this.state.formControls.gender.touched}
                         valid={this.state.formControls.gender.valid}
              />
              {this.state.formControls.gender.touched && !this.state.formControls.gender.valid &&
              <div className={"error-message row"}> * Name must have at least 3 characters </div>}

              <p> Address: </p>
              <TextInput name="address"
                         placeholder={this.state.formControls.address.placeholder}
                         value={this.state.formControls.address.value}
                         onChange={this.handleChange}
                         touched={this.state.formControls.address.touched}
                         valid={this.state.formControls.address.valid}
              />
              {this.state.formControls.address.touched && !this.state.formControls.address.valid &&
              <div className={"error-message row"}> * Name must have at least 3 characters </div>}

              <p> username: </p>
              <TextInput name="username"
                         placeholder={this.state.formControls.username.placeholder}
                         value={this.state.formControls.username.value}
                         onChange={this.handleChange}
                         touched={this.state.formControls.username.touched}
                         valid={this.state.formControls.username.valid}
              />

              <p> password: </p>
              <TextInput name="password"
                         placeholder={this.state.formControls.password.placeholder}
                         value={this.state.formControls.password.value}
                         onChange={this.handleChange}
                         touched={this.state.formControls.password.touched}
                         valid={this.state.formControls.password.valid}
              />

              <p> </p>
              <Button variant="success"
                      type={"submit"}
                      disabled={!this.state.formIsValid}>
                  Submit
              </Button>
              &nbsp;
              <Button variant="success" onClick={() => this.deleteCaregiver(this.state.formControls.id.value)}>
                  Delete
              </Button>
              &nbsp;
              <Button variant="success" onClick={() => this.updateCaregiver()}>
                  Update
              </Button>

              {this.state.errorStatus > 0 &&
              <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>}

          </form>
    );
    }
}

export default PersonForm;
