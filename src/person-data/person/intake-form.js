import React from 'react';
import validate from "./validators/person-validators";
import TextInput from "./fields/TextInput";
import './fields/fields.css';
import Button from "react-bootstrap/Button";
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";
import ApiService from "../../api-service";


class IntakeForm extends React.Component{

    constructor(props){
        super(props);
        this.toggleForm = this.toggleForm.bind(this);

        this.state = {

            errorStatus: 0,
            error: null,

            formIsValid: false,

            formControls : {
                startDate: {
                    value: '',
                    placeholder: 'YYYY-MM-DD',
                    valid: false,
                    touched: false,
                    validationRules: {
                        minLength: 3,
                        isRequired: true
                    }
                },

                endDate: {
                    value: '',
                    placeholder: 'YYYY-MM-DD',
                    valid: false,
                    touched: false,
                    validationRules: {
                        isRequired: true
                    }
                },

                medicationId: {
                    value: '',
                    placeholder: '',
                    valid: false,
                    touched: false,

                },
                patientId: {
                    value: '',
                    placeholder: '',
                    valid: false,
                    touched: false,

                },
            }
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmitIntake = this.handleSubmitIntake.bind(this);
    }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }

    componentDidMount() {

    }


    handleChange = event => {

        const name = event.target.name;
        const value = event.target.value;

        const updatedControls = {
            ...this.state.formControls
        };

        const updatedFormElement = {
            ...updatedControls[name]
        };

        updatedFormElement.value = value;
        updatedFormElement.touched = true;
        updatedFormElement.valid = validate(value, updatedFormElement.validationRules);

        console.log("Element: " +  name + " validated: " + updatedFormElement.valid);

        updatedControls[name] = updatedFormElement;

        let formIsValid = true;
        for (let updatedFormElementName in updatedControls) {
            formIsValid = updatedControls[updatedFormElementName].valid && formIsValid;
        }

        this.setState({
            formControls: updatedControls,
            formIsValid: formIsValid
        });
    };

    handleSubmitIntake(){
        let intake = {
            startDate: this.state.formControls.startDate.value,
            endDate: this.state.formControls.endDate.value,
            medicationId: this.state.formControls.medicationId.value,
            patientId: this.state.formControls.patientId.value
        };
        ApiService.insertIntake(intake);
    }

    render() {
        return (

            <form onSubmit={this.handleSubmitIntake}>

                <h1>Insert new Intake</h1>

                <p> Start Date: </p>

                <TextInput name="startDate"
                           placeholder={this.state.formControls.startDate.placeholder}
                           value={this.state.formControls.startDate.value}
                           onChange={this.handleChange}
                           touched={this.state.formControls.startDate.touched}
                           valid={this.state.formControls.startDate.valid}
                />


                <p> End Date: </p>

                <TextInput name="endDate"
                           placeholder={this.state.formControls.endDate.placeholder}
                           value={this.state.formControls.endDate.value}
                           onChange={this.handleChange}
                           touched={this.state.formControls.endDate.touched}
                           valid={this.state.formControls.endDate.valid}
                />
                {this.state.formControls.endDate.touched && !this.state.formControls.endDate.valid &&
                <div className={"error-message row"}> * Name must have at least 3 characters </div>}

                <p> medication id: </p>
                <TextInput name="medicationId"
                           placeholder={this.state.formControls.medicationId.placeholder}
                           value={this.state.formControls.medicationId.value}
                           onChange={this.handleChange}
                           touched={this.state.formControls.medicationId.touched}
                           valid={this.state.formControls.medicationId.valid}
                />
                {this.state.formControls.medicationId.touched && !this.state.formControls.medicationId.valid &&
                <div className={"error-message row"}> * Name must have at least 3 characters </div>}

                <p> patient id: </p>
                <TextInput name="patientId"
                           placeholder={this.state.formControls.patientId.placeholder}
                           value={this.state.formControls.patientId.value}
                           onChange={this.handleChange}
                           touched={this.state.formControls.patientId.touched}
                           valid={this.state.formControls.patientId.valid}
                />
                {this.state.formControls.patientId.touched && !this.state.formControls.patientId.valid &&
                <div className={"error-message row"}> * Name must have at least 3 characters </div>}


                <p> </p>
                <Button variant="success"
                        type={"submit"}
                        disabled={!this.state.formIsValid}>
                    Submit
                </Button>

                {this.state.errorStatus > 0 &&
                <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>}

            </form>
        );
    }
}

export default IntakeForm;
