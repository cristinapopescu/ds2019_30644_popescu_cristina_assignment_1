import React, { Component } from 'react'
import PersonForm from "./person-form";
import ApiService from "../../api-service"
import CaregiverForm from "./caregiver-form";
import MedicationForm from "./medication-form";
import IntakeForm from "./intake-form";

class Persons extends Component {
    constructor(props) {
        super(props)
        this.state = {
            caregivers: [],
            patients: [],
            medications: [],

        };
        this.refreshMed = this.refreshMed.bind(this);
        this.refreshPatient = this.refreshPatient.bind(this);
        this.refreshMedication = this.refreshMedication.bind(this);
    }

    componentDidMount() {
        this.refreshMed();
        this.refreshPatient();
        this.refreshMedication();
    }

    refreshMed() {
        ApiService.getCaregiver()
            .then(
                response => {
                    this.setState({ caregivers: response.data })
                }
            )
    }

    refreshPatient() {
        ApiService.getPatient()
            .then(
                response => {
                    this.setState({ patients: response.data })
                }
            )
    }

    refreshMedication() {
        ApiService.getMedication()
            .then(
                response => {
                    this.setState({ medications: response.data })
                }
            )
    }

    render() {
        console.log('render');
        if (localStorage.getItem('persons') === "doctor") {
            return (
                <div className="container">
                    <h3>Caregiver</h3>
                    {this.state.message && <div class="alert alert-success">{this.state.message}</div>}
                    <div className="container">
                        <table className="table">
                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Name</th>
                                <th>Birth date</th>
                                <th>Address</th>
                                <th>Gender</th>
                            </tr>
                            </thead>
                            <tbody>
                            {
                                this.state.caregivers.map(
                                    caregiver =>
                                        <tr key={caregiver.id}>
                                            <td>{caregiver.id}</td>
                                            <td>{caregiver.name}</td>
                                            <td>{caregiver.birthDate}</td>
                                            <td>{caregiver.address}</td>
                                            <td>{caregiver.gender}</td>
                                        </tr>
                                )
                            }
                            </tbody>
                        </table>

                    </div>

                    <PersonForm/>

                    <br/>
                    <h3>Patient</h3>
                    <div className="container">
                        <table className="table">
                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Name</th>
                                <th>Birth date</th>
                                <th>Address</th>
                                <th>Gender</th>
                            </tr>
                            </thead>
                            <tbody>
                            {
                                this.state.patients.map(
                                    patient =>
                                        <tr key={patient.id}>
                                            <td>{patient.id}</td>
                                            <td>{patient.name}</td>
                                            <td>{patient.birthDate}</td>
                                            <td>{patient.address}</td>
                                            <td>{patient.gender}</td>
                                            <td>{patient.medicalRecord}</td>
                                        </tr>
                                )
                            }
                            </tbody>
                        </table>

                    </div>
                    <CaregiverForm/>

                    <br/>

                    <h3>Medication</h3>
                    <div className="container">
                        <table className="table">
                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Side effects</th>
                                <th>Dosage</th>
                                <th>Name</th>
                            </tr>
                            </thead>
                            <tbody>
                            {
                                this.state.medications.map(
                                    medication =>
                                        <tr key={medication.id}>
                                            <td>{medication.id}</td>
                                            <td>{medication.dosage}</td>
                                            <td>{medication.sideEffects}</td>
                                            <td>{medication.name}</td>
                                        </tr>
                                )
                            }
                            </tbody>
                        </table>

                    </div>
                    <MedicationForm/>
                    <br/>
                    <IntakeForm/>
                    <br/>
                </div>
            )
        } else {
            return (
                <div>
                    <p>nu-i voie</p>
                </div>
            )
        }
    }
}

export default Persons;
