import React, { Component } from 'react'
import ApiService from './api-service'

class CaregiverPatients extends Component {
    constructor(props) {
        super(props)
        this.state = {
            patients: [],
        };
        this.refreshPatient = this.refreshPatient.bind(this);
    }

    componentDidMount() {
        this.refreshPatient();
    }

    refreshPatient() {
        ApiService.getCaregiverPatients()
            .then(
            response => {
                this.setState({ patients: response.data })
            }
        )
    }

    render() {
        console.log('render');
        if (localStorage.getItem('role') === "caregiver") {
            return (
                <div className="container">
                    <h3>Patients</h3>
                    {this.state.message && <div class="alert alert-success">{this.state.message}</div>}
                    <div className="container">
                        <table className="table">
                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Name</th>
                                <th>Birth date</th>
                                <th>Address</th>
                                <th>Gender</th>
                                <th>MedicalRecord</th>
                            </tr>
                            </thead>
                            <tbody>
                            {
                                this.state.patients.map(
                                    patient =>
                                        <tr key={patient.id}>
                                            <td>{patient.id}</td>
                                            <td>{patient.name}</td>
                                            <td>{patient.birthDate}</td>
                                            <td>{patient.address}</td>
                                            <td>{patient.gender}</td>
                                            <td>{patient.medicalRecord}</td>
                                        </tr>
                                )
                            }
                            </tbody>
                        </table>

                    </div>

                </div>
            )
        }else {
            return(
                <div>
                    <p>nu-i voie</p>
                </div>
                )

        }
    }
}

export default CaregiverPatients;
