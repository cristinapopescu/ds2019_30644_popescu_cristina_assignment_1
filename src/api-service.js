import axios from 'axios'

class ApiService {
    login(data) {
        return axios.post(`http://localhost:8080/login`, data);
    }

    insertCaregiver(caregiver) {
        return axios.post(`http://localhost:8080/caregiver`, caregiver);
    }
    getCaregiver() {
        return axios.get(`http://localhost:8080/caregiver`);
    }

    updateCaregiver(caregiver) {
        return axios.put(`http://localhost:8080/caregiver`, caregiver);
    }
    deleteCaregiver(id) {
        return axios.delete(`http://localhost:8080/caregiver/` + id);
    }

    insertPatient(patient){
        return axios.post(`http://localhost:8080/patient`, patient);
    }
    getPatient(){
        return axios.get(`http://localhost:8080/patient`);
    }
    deletePatient(id){
        return axios.delete(`http://localhost:8080/patient/` + id);
    }
    updatePatient(patient){
        return axios.put(`http://localhost:8080/patient`, patient);
    }

    insertMedication(medication){
        return axios.post(`http://localhost:8080/medication`, medication);
    }
    getMedication(){
        return axios.get(`http://localhost:8080/medication`);
    }
    deleteMedication(id){
        return axios.delete(`http://localhost:8080/medication/` + id);
    }
    updateMedication(medication){
        return axios.put(`http://localhost:8080/medication`, medication);
    }
    insertIntake(intake){
        return axios.post(`http://localhost:8080/intake`, intake);
    }
    getCaregiverPatients(){
        return axios.get(`http://localhost:8080/caregiver/` + localStorage.getItem('userId'))
    }
}
export default new ApiService()